﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EditorWin.Controller;
using System.Data;
using System.IO;
using System.Threading;
using EditorWin.Model;

namespace EditorWin.View
{
    public partial class frmEditor : Form
    {
        public byte[] logoEscola;
        public int idLoginProfessor = -1;
        private PrincipalController principalController;
        public frmLogin frlogin;
        TableLayoutPanel pnl;
        public frmEditor()
        {
            InitializeComponent();
            textBox1.Text = "Universidade Tecnológica Federal do Paraná - UTFPR";
            LoadAutoComplete();
        }

        private void LoadAutoComplete()
        {

            AutoCompleteStringCollection dadosLista = new AutoCompleteStringCollection();

            WordList ws = new WordList();
            var words = ws.getWordsAutoComplete();

            foreach( string ls in words)
            {
                dadosLista.Add(ls);
            }

            edtFindWord.AutoCompleteMode = AutoCompleteMode.Suggest;
            edtFindWord.AutoCompleteCustomSource = dadosLista;
        }

        public void setProfessor(int pr)
        {
            principalController = new PrincipalController();
            principalController.parametros.userParametros.ID_PROFESSOR = pr;
            idLoginProfessor = pr;
        }

        //---------------
        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        public static extern long BitBlt(IntPtr hdcDest, int nXDest, int nYDest, int nWidth, int nHeight, IntPtr hdcSrc, int nXSrc, int nYSrc, int dwRop);
        private Bitmap memoryImage;
        private void CaptureScreen()
        {
            Graphics mygraphics = this.CreateGraphics();
            Size si = this.Size;
            Size s = this.panelToPrint.Size;
            memoryImage = new Bitmap(s.Width, s.Height, mygraphics);
            Graphics memoryGraphics = Graphics.FromImage(memoryImage);
            IntPtr dc1 = mygraphics.GetHdc();
            IntPtr dc2 = memoryGraphics.GetHdc();
            BitBlt(dc2, 0, 0, this.panelToPrint.Width, this.panelToPrint.Height, dc1, 0, 0, 13369376);
            //BitBlt(dc2, 0, 0, this.ClientRectangle.Width, this.ClientRectangle.Height, dc1, 0, 0, 13369376);
            mygraphics.ReleaseHdc(dc1);
            memoryGraphics.ReleaseHdc(dc2);
        }

        public static void InvertZOrderOfControls(Control.ControlCollection ControlList)
        {
            // do not process empty control list
            if (ControlList.Count == 0)
                return;
            // only re-order if list is writable
            if (!ControlList.IsReadOnly)
            {
                SortedList<int, Control> sortedChildControls = new SortedList<int, Control>();
                // find all none docked controls and sort in to list
                foreach (Control ctrlChild in ControlList)
                {
                    if (ctrlChild.Dock == DockStyle.None)
                        sortedChildControls.Add(ControlList.GetChildIndex(ctrlChild), ctrlChild);
                }
                // re-order the controls in the parent by swapping z-order of first 
                // and last controls in the list and moving towards the center
                for (int i = 0; i < sortedChildControls.Count / 2; i++)
                {
                    Control ctrlChild1 = sortedChildControls.Values[i];
                    Control ctrlChild2 = sortedChildControls.Values[sortedChildControls.Count - 1 - i];
                    int zOrder1 = ControlList.GetChildIndex(ctrlChild1);
                    int zOrder2 = ControlList.GetChildIndex(ctrlChild2);
                    ControlList.SetChildIndex(ctrlChild1, zOrder2);
                    ControlList.SetChildIndex(ctrlChild2, zOrder1);
                }
            }
            // try to invert the z-order of child controls
            foreach (Control ctrlChild in ControlList)
            {
                try { InvertZOrderOfControls(ctrlChild.Controls); }
                catch { }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void CarregarListBoxUsuario()
        {
            DataSet dsUsuario = new DataSet();
            dsUsuario = principalController.getListaUsuario(idLoginProfessor);
            dataGridView1.DataSource = dsUsuario.Tables[0].DefaultView;

            DataTable tblUsuario = dsUsuario.Tables[0];
            listBoxUsuario.Items.Clear();
            foreach (DataRow rw in tblUsuario.Rows)
            {
                listBoxUsuario.Items.Add(rw.Field<string>(1));
            }
            if (listBoxUsuario.Items.Count > 0) {
                listBoxUsuario.SetSelected((listBoxUsuario.Items.Count) - 1, true);
                string us;
                us = listBoxUsuario.GetItemText(listBoxUsuario.SelectedItem);
                popularListBomArranjos(us);
            }
        }
        public void ImprimirCabecalhoPrancha(bool imprime) {
            edtIdentificaEscola.Text = principalController.parametros.userParametros.TEXTO_TO_USER;
            chkImprimir.Checked = principalController.parametros.userParametros.Imprimir;
            if (principalController.parametros.userParametros.IMAGE != null)
            {
                logoSelecionada.SizeMode = PictureBoxSizeMode.StretchImage;
                logoSelecionada.Image = ByteToImage(principalController.parametros.userParametros.IMAGE);
                imgLogoEscolaPrint.Image = ByteToImage(principalController.parametros.userParametros.IMAGE);
                imgLogoEscolaPrint.SizeMode = PictureBoxSizeMode.StretchImage;
            }
            lbNomeEscolaPrint.Text = edtIdentificaEscola.Text;
            if (imprime)
            {
                PanelCabecalho.Visible = true;
            } else
            {
                PanelCabecalho.Visible = false;
            }
            lbNomeArranjo.Text = "";
            tableLayoutPanel1.Controls.Clear();
            carregarArranjoSelecionado();
        }


        public void AtivarFormEditor()
        {
            this.WindowState = FormWindowState.Maximized;
            CarregarListBoxUsuario();
            principalController.getParametrosProfessor(idLoginProfessor);
            ImprimirCabecalhoPrancha(principalController.parametros.userParametros.Imprimir);
            //  frlogin.Close();

        }

        private void frmEditor_Shown(object sender, EventArgs e)
        {
            tableLayoutPanel1.Hide();
            if (idLoginProfessor != -1) {
                AtivarFormEditor();
                tableLayoutPanel1.Controls.Clear();
                tableLayoutPanel1.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
                BuscarPalavras("am");
            }
            else
            {
                frlogin = new frmLogin();
                frlogin.setfrm(this);
                frlogin.Show();
                this.Hide();
            }
            cbPrintFormat.SelectedIndex = 1;
            if (listArranjos.Items.Count > 0)
                listArranjos.SetSelected((listArranjos.Items.Count) - 1, true);
            tableLayoutPanel1.Show();

        }

        private void chProfessor_CheckedChanged(object sender, EventArgs e)
        {
            chAluno.Checked = !chProfessor.Checked;
        }

        private void chAluno_Click(object sender, EventArgs e)
        {
            chProfessor.Checked = !chAluno.Checked;
        }

        private void btnGravarUsuario_Click(object sender, EventArgs e)
        {
            UsuarioController us = new UsuarioController();
            bool Perfil = false;
            if (chProfessor.Checked)
                Perfil = true;

            MessageBox.Show(principalController.InserirUsuario(edtNome.Text, edtLogin.Text, Perfil, idLoginProfessor));

            UsuarioController uss = new UsuarioController();
            dataGridView1.DataSource = uss.ListaUsuarios(idLoginProfessor).Tables[0].DefaultView;
            dataGridView1.Columns.RemoveAt(0);
            CarregarListBoxUsuario();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //  MessageBox.Show("");
        }



        public static Bitmap ByteToImage(byte[] blob)
        {
            MemoryStream mStream = new MemoryStream();
            byte[] pData = blob;
            mStream.Write(pData, 0, Convert.ToInt32(pData.Length));
            Bitmap bm = new Bitmap(mStream, false);
            mStream.Dispose();
            return bm;
        }

        private ImageList loadImageList()
        {
            ImageList IMLIST = new ImageList();
            foreach (IMAGEM i in principalController.imgList)
            {
                Image a;
                a = ByteToImage(i.img);
                IMLIST.Images.Add(i.ID_IMAGEM.ToString(), a);
            }
            return IMLIST;
        }

        private void edtFindWord_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void listBox3_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void edtFindWord_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        public string getName(int l, int c)
        {
            string name;

            string linha = l.ToString();
            while (linha.Length < 4)
            {
                linha = '0' + linha;
            }
            string coluna = c.ToString();
            while (coluna.Length < 4)
            {
                coluna = '0' + coluna;
            }
            name = linha + '_' + coluna;
            return name;
        }

        public void buildLayout()
        {
            List<IMAGEM> im = new List<IMAGEM>();
            WordList wl = new WordList();
            im = wl.getWordList(edtFindWord.Text);
            string nameCheck;
            int numimg = 0;
            {
                tableLayoutPanel1.SuspendLayout();
                tableLayoutPanel1.Controls.Clear();
                for (int l = 0; l < int.Parse(edtLinhas.Text); l++)
                {
                    for (int c = 0; c < int.Parse(edtColunas.Text); c++)
                    {
                        nameCheck = getName(l, c);
                        CheckBox ch = new CheckBox();
                        ch.Name = nameCheck;
                        if (numimg < im.Count()) {
                            PictureBox pc = new PictureBox();
                            pc.Image = ByteToImage(im[numimg].img);
                            numimg++;
                            pc.SizeMode = PictureBoxSizeMode.StretchImage;
                            pc.Dock = DockStyle.Fill;
                            Button btn = new Button();
                            NovoPanel(l, c);
                        }
                    }
                }
                tableLayoutPanel1.ResumeLayout(false);
                tableLayoutPanel1.PerformLayout();
            }
        }
        private void chk_posicao(object sender, EventArgs e)
        {
            var chk = (CheckBox)sender;
            principalController.posLinha = Convert.ToInt32(chk.Name.Substring(0, 4));
            principalController.posColuna = Convert.ToInt32(chk.Name.Substring(5, 4));

            for (int l = 0; l <= int.Parse(edtLinhas.Text); l++)
            {
                for (int c = 0; c <= int.Parse(edtColunas.Text); c++)
                {
                    foreach (Panel nch in tableLayoutPanel1.Controls.OfType<Panel>())
                    {
                        foreach (CheckBox ch in nch.Controls.OfType<CheckBox>())
                        {
                            ch.Checked = false;
                        }
                    }
                }
            }
            chk.Checked = true;
        }

        public void NovoPanel(int l, int c)
        {
            Panel pn = new Panel();
            pn.Dock = DockStyle.Fill;
            PictureBox pc = new PictureBox();
            pc.SizeMode = PictureBoxSizeMode.CenterImage;
            pc.Dock = DockStyle.Fill;
            CheckBox chk = new CheckBox();
            chk.Name = getName(l, c);
            chk.Click += chk_posicao;
            //chk.Text = "Posição:" + l.ToString() + " / " + c.ToString() + "";
            pn.Controls.Add(chk);
            pn.Controls.Add(pc);
            pn.Enabled = true;

            TextBox txtlabel = new TextBox();
            txtlabel.Dock = DockStyle.Bottom;
            txtlabel.TextAlign = HorizontalAlignment.Center;
            txtlabel.CharacterCasing = CharacterCasing.Upper;

            pn.Controls.Add(txtlabel);

            tableLayoutPanel1.Controls.Add(pn, c, l);
        }

        public void MontarLayout(int linhas, int colunas)
        {
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel1.AutoSize = false;
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.TabIndex = 0;
            this.tableLayoutPanel1.RowStyles.Clear();
            this.tableLayoutPanel1.ColumnStyles.Clear();
            tableLayoutPanel1.SuspendLayout();
            tableLayoutPanel1.Controls.Clear();

            for (int l = 0; l <= linhas; l++)
            {
                tableLayoutPanel1.RowCount = l;
                this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(SizeType.Percent, 50));
                for (int c = 0; c <= colunas; c++)
                {
                    if (l == 0)
                    {
                        tableLayoutPanel1.ColumnCount = c;
                        this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50));
                    }

                }
            }
            tableLayoutPanel1.ResumeLayout(false);
            tableLayoutPanel1.PerformLayout();
        }

        private void ListViewManager()
        {
            if (listView1.InvokeRequired)
            {
                listView1.Invoke(new MethodInvoker( delegate
                {
                    listView1.LargeImageList = IMLIST;
                    listView1.Items.Clear();

                    foreach (IMAGEM img in principalController.imgList)
                    {
                        ListViewItem l = new ListViewItem();
                        l.ImageKey = img.ID_IMAGEM.ToString();
                        l.Text = img.TEXTO_TO_USER;
                        listView1.Items.Add(l);
                    }
                }));
            }
        }
        private ImageList IMLIST;
        private void managerListWords()
        {
            principalController.getListPalavras(word, principalController.tipoBusca);
             IMLIST = new ImageList();
            IMLIST = loadImageList();
            IMLIST.ImageSize = new Size(75, 75);
        }
        private string word;
        private void BuscarPalavras(string wd) {
            word = wd;
            if (chkIniciais.Checked)
            {
                principalController.tipoBusca = TipoBusca.StartWith;
            }
            else
            {
                principalController.tipoBusca = TipoBusca.Contains;
            }
            //            managerListWords();
            if (edtFindWord.Text.Length >= 0)
            {
                managerListWords();


                Thread tr = new Thread(ListViewManager);
                if (tr.ThreadState == ThreadState.Running)
                    tr.Abort();
                tr.Start();

                /*
                 foreach (IMAGEM img in principalController.imgList)
                {
                    ListViewItem l = new ListViewItem();
                    l.ImageKey = img.TEXTO_TO_USER;
                    l.Text = img.TEXTO_TO_USER;
                    listView1.Items.Add(l);
                }
                */
                if (principalController.imgList.Count > 0)
                {
                    lbCorresp.Text = "Encontradas " + principalController.imgList.Count.ToString() + " figuras";
                }
                else
                {
                    lbCorresp.Text = "Encontradas " + principalController.imgList.Count.ToString() + " figuras";
                    lbCorresp.ForeColor = Color.Red;
                }
            }

        }


        private void btnFindImagens_Click_1(object sender, EventArgs e)
        {
            List<IMAGEM> im = new List<IMAGEM>();
            WordList wl = new WordList();
            im = wl.getWordList(edtFindWord.Text);

            buildLayout();
        }
        public void novoArranjo()
        {
            tableLayoutPanel1.SuspendLayout();
            tableLayoutPanel1.Controls.Clear();
            var r = principalController.ValidarArranjo(edtNomePrancha.Text, listBoxUsuario.Items[listBoxUsuario.SelectedIndex].ToString(), idLoginProfessor, int.Parse(edtLinhas.Text), int.Parse(edtColunas.Text));
            if (!r)
            {
                principalController.AddArranjo(edtNomePrancha.Text, listBoxUsuario.Items[listBoxUsuario.SelectedIndex].ToString(), idLoginProfessor, int.Parse(edtLinhas.Text), int.Parse(edtColunas.Text));
                popularListBomArranjos(listBoxUsuario.Items[listBoxUsuario.SelectedIndex].ToString());
            }
            MontarLayout(int.Parse(edtLinhas.Text), int.Parse(edtColunas.Text));


            for (int linhas = 0; linhas < int.Parse(edtLinhas.Text); linhas++)
            {
                for (int colunas = 0; colunas < int.Parse(edtColunas.Text); colunas++)
                {
                    NovoPanel(linhas, colunas);
                }
            }

            tableLayoutPanel1.ResumeLayout(false);
            tableLayoutPanel1.PerformLayout();
        }

        private void btnGerarArranjo_Click(object sender, EventArgs e)
        {
            try
            {
                if (edtNomePrancha.Text == "")
                {
                    MessageBox.Show("Campo nome do arranjo não esta preenchido. Operação cancelada.");
                    return;
                }
                novoArranjo();
                listArranjos.SetSelected((listArranjos.Items.Count - 1), true);
            }
            catch {
                MessageBox.Show(" O numero de colunas ou linhas não esta preenchido corretamente. Operação cancelada.");
            }

        }


        public void setImagemPosicao(byte[] pc)
        {
            var c = (Panel)tableLayoutPanel1.GetControlFromPosition(principalController.posColuna, principalController.posLinha);
            foreach (PictureBox p in c.Controls.OfType<PictureBox>())
                p.Image = ByteToImage(pc);
        }

        private void listView1_Click(object sender, EventArgs e)
        {
            if (principalController.posLinha > -1) {
                ListViewItem li = new ListViewItem();
                li = listView1.SelectedItems[0];
                var c = (Panel)tableLayoutPanel1.GetControlFromPosition(principalController.posColuna, principalController.posLinha);
                foreach (PictureBox p in c.Controls.OfType<PictureBox>())
                {
                    Image img;
                    img = ByteToImage(principalController.getImagem(li.Text).img);

                    p.Image = ByteToImage(principalController.getImagem(li.Text).img);
                    p.Tag = principalController.getImagem(li.Text).ID_IMAGEM;
                    //    float perc = principalController.getPercentual(ByteToImage(principalController.getImagem(li.Text).img).Width, ByteToImage(principalController.getImagem(li.Text).img).Height, ((c.Height - 26)));
                    //    float nAltura = principalController.getPercentual(ByteToImage(principalController.getImagem(li.Text).img).Height, ByteToImage(principalController.getImagem(li.Text).img).Width, (perc));
                    float perc = 0;
                    float nAltura = 0;
                    if (c.Height < c.Width)
                    {
                        perc = principalController.getPercentual(img.Width, img.Height, ((c.Height - 26)));
                        nAltura = principalController.getPercentual(img.Height, img.Width, (perc));
                    }
                    else
                    {
                        perc = principalController.getPercentual(img.Width, img.Height, ((c.Width - 26)));
                        nAltura = principalController.getPercentual(img.Height, img.Width, (perc));
                    }
                    float atual = ByteToImage(principalController.getImagem(li.Text).img).Width;
                    p.Dock = DockStyle.None;
                    float ponIni = ((c.Width - perc) / 2);

                    p.Location = new Point((int)ponIni, 0);
                    p.Size = new System.Drawing.Size((int)(perc), (int)(nAltura));
                    p.Size = new Size((int)(perc), (int)(nAltura));
                    p.SizeMode = PictureBoxSizeMode.StretchImage;
                    c.Dock = DockStyle.Fill;
                }
                foreach (TextBox txtBox in c.Controls.OfType<TextBox>())
                {
                    txtBox.Text = principalController.getImagem(li.Text).TEXTO_TO_USER;
                    //   txtBox.Font = new Font(new FontFamily("Arial"), 12);
                }
            }
        }

        public void TratarCamposVisivelImpressao(bool rs)
        {
            foreach (Panel pn in tableLayoutPanel1.Controls.OfType<Panel>())
            {
                foreach (CheckBox p in pn.Controls.OfType<CheckBox>())
                {
                    p.Visible = rs;
                }
            }
        }



        private int getHeigth()
        {
            int largura;
            int altura;
            if (cbPrintFormat.Text == "Retrato")
            {
                largura = 827;
                altura = 1168;
                if (PanelCabecalho.Visible == true)
                {
                    altura = altura - PanelCabecalho.Height;
                }
            }
            else
            {
                largura = 1168;
                altura = 827;
                if (PanelCabecalho.Visible == true)
                {
                    altura = altura - PanelCabecalho.Height;
                }
            }
            int novaH = (altura / int.Parse(edtLinhas.Text));
            int novaW = (largura / int.Parse(edtColunas.Text));

            if (novaH > novaW)
            {
                novaH = novaW * int.Parse(edtLinhas.Text);
                novaH = (novaH - (int.Parse(edtLinhas.Text)));
            }
            else
            {
                novaH = novaH * int.Parse(edtLinhas.Text);
                novaH = (novaH - (int.Parse(edtLinhas.Text)));
            }


            if (cbPrintFormat.Text == "Retrato")
            {
                if (PanelCabecalho.Visible)
                    if ((novaH + PanelCabecalho.Height) > 1168)
                    {
                        novaH = novaH - PanelCabecalho.Height;
                    }
                    else
                    {
                        novaH = novaH + PanelCabecalho.Height;
                    }
                panelToPrint.Size = new Size(827, novaH);
            }
            else
            {
                if (PanelCabecalho.Visible)
                    if ((novaH + PanelCabecalho.Height) > 827)
                    {
                        novaH = novaH - PanelCabecalho.Height;
                    }
                    else
                    {
                        novaH = novaH + PanelCabecalho.Height;
                    }
                panelToPrint.Size = new Size(1160, novaH);
            }
            return 0;
        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            panelToPrint.Padding = new Padding(30, 30, 30, 30);
            getHeigth();

            if (!principalController.parametros.userParametros.Imprimir)
                PanelCabecalho.Visible = false;
            panelToPrint.Dock = DockStyle.None;
            PanelCabecalho.Dock = DockStyle.Top;

         //   panelToPrint.Size = new Size(827, 1168);
            carregarArranjoSelecionado();
            TratarCamposVisivelImpressao(false);

            Bitmap image = new Bitmap(panelToPrint.Width, panelToPrint.Height);
            image.SetResolution(100, 100);
            InvertZOrderOfControls(panelToPrint.Controls);
            panelToPrint.DrawToBitmap(image, new Rectangle(new Point(), panelToPrint.Size));
            InvertZOrderOfControls(panelToPrint.Controls);

            e.Graphics.DrawImage(image, 0, 0);
            panelToPrint.Dock = DockStyle.Fill;
            if (principalController.parametros.userParametros.Imprimir == true)
            {
                PanelCabecalho.Visible = true;
            }
            TratarCamposVisivelImpressao(true);
            panelToPrint.Padding = new Padding(5, 5, 5, 5);
            carregarArranjoSelecionado();
        }

        private void btnSelLogo_Click(object sender, EventArgs e)
        {
            fileSelLogo.InitialDirectory = "c:";
            fileSelLogo.Filter = "jpg (*.jpg)|*.jpg|jpeg (*.jpeg)|*.jpeg";
            if (fileSelLogo.ShowDialog() == DialogResult.OK) {
                logoEscola = File.ReadAllBytes(fileSelLogo.FileName);
                logoSelecionada.SizeMode = PictureBoxSizeMode.StretchImage;
                logoSelecionada.Image = ByteToImage(logoEscola);
                Image img;
                img = ByteToImage(logoEscola);
                float perc = principalController.getPercentual(img.Width, img.Height, ((100)));
                float nAltura = principalController.getPercentual(img.Height, img.Width, (perc));
                logoSelecionada.Dock = DockStyle.None;
                logoSelecionada.Size = new System.Drawing.Size((int)(perc), (int)(nAltura));
                logoSelecionada.Size = new Size((int)(perc), (int)(nAltura));
                logoSelecionada.SizeMode = PictureBoxSizeMode.StretchImage;
                //*******************
                imgLogoEscolaPrint.Image = ByteToImage(logoEscola);
                imgLogoEscolaPrint.Dock = DockStyle.Right;
                imgLogoEscolaPrint.Size = new System.Drawing.Size((int)(perc), (int)(nAltura));
                imgLogoEscolaPrint.Size = new Size((int)(perc), (int)(nAltura));
                imgLogoEscolaPrint.SizeMode = PictureBoxSizeMode.StretchImage;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                principalController.InserirParametro(edtIdentificaEscola.Text, logoEscola, chkImprimir.Checked, idLoginProfessor);
                principalController.getParametrosProfessor(idLoginProfessor);
                ImprimirCabecalhoPrancha(principalController.parametros.userParametros.Imprimir);
                MessageBox.Show("Informações gravadas com sucesso.");
            }
            catch {

            }
        }

        public void popularListBomArranjos(string sLogin)
        {
            listArranjos.Items.Clear();
            foreach (Arranjo ar in principalController.getArranjos(sLogin))
            {
                listArranjos.Items.Add(ar.NomeArranjo);
            }
            if (listArranjos.Items.Count == 0)
            {
                edtColunas.Text = "";
                edtLinhas.Text = "";
                edtNomePrancha.Text = "";
                tableLayoutPanel1.Visible = false;
            } else
            {
                tableLayoutPanel1.Visible = true;

            }
        }

        private void listBoxUsuario_Click(object sender, EventArgs e)
        {
            if (listBoxUsuario.Items.Count > 0)
            {
                btnNovaPrancha.Enabled = true;
                popularListBomArranjos(listBoxUsuario.Items[listBoxUsuario.SelectedIndex].ToString());
            }
        }


        private void btnNovaPrancha_Click(object sender, EventArgs e)
        {
            if (listBoxUsuario.SelectedIndex > -1)
            {
                edtNomePrancha.Enabled = true;
                edtNomePrancha.Text = "";
                edtLinhas.Enabled = true;
                edtLinhas.Text = "";
                edtColunas.Enabled = true;
                edtColunas.Text = "";
                btnGerarArranjo.Enabled = true;
            }
        }

        private void ExcluirArranjo()
        {
            if ((listArranjos.SelectedIndex > -1) && (listBoxUsuario.SelectedIndex > -1))
            {
                string user = listBoxUsuario.Items[listBoxUsuario.SelectedIndex].ToString();
                string nome = listArranjos.Items[listArranjos.SelectedIndex].ToString();
                principalController.ExcluirArranjo(nome, user, idLoginProfessor);
                popularListBomArranjos(user);
            };
        }

        private void btnExcluirArranjo_Click(object sender, EventArgs e)
        {
            try
            {
                ExcluirArranjo();
                MessageBox.Show("Arranjo Excluído com sucesso.");
                if (listArranjos.Items.Count > 0) {
                    listArranjos.SetSelected((listArranjos.Items.Count - 1), true);
                } else
                {
                    edtNomePrancha.Enabled = false;
                    edtNomePrancha.Text = "";
                    edtLinhas.Enabled = false;
                    edtLinhas.Text = "";
                    edtColunas.Enabled = false;
                    edtColunas.Text = "";
                    btnGerarArranjo.Enabled = false;
                }
            }
            catch
            {
                MessageBox.Show("Falha ao excluir arranjo.");
            }
        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {

            if (cbPrintFormat.Text == "Retrato")
            {
                printDocument1.DefaultPageSettings.Landscape = false;
            }
            else
            {
                printDocument1.DefaultPageSettings.Landscape = true;
            }

            CaptureScreen();


            if (printDialog1.ShowDialog() == DialogResult.OK)
                printDocument1.Print();

        }

        private void edtLinhas_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && (e.KeyChar != 8))
            {
                e.Handled = true;
            }
        }

        private void edtColunas_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && (e.KeyChar != 8))
            {
                e.Handled = true;
            }
        }

        public void setPictureImageContent(PictureBox p, Panel c) {
            c.Dock = DockStyle.Fill;
            Image img;
            img = ByteToImage(principalController.getImagem(principalController.elemento.elemento.ID_IMAGEM).img);
            float perc = 0;
            float nAltura = 0;
            if (c.Height < c.Width)
            {
                perc = principalController.getPercentual(img.Width, img.Height, ((c.Height - 26)));
                nAltura = principalController.getPercentual(img.Height, img.Width, (perc));
            }
            else
            {
                perc = principalController.getPercentual(img.Width, img.Height, ((c.Width - 26)));
                nAltura = principalController.getPercentual(img.Height, img.Width, (perc));
            }
            p.Dock = DockStyle.None;
            float ponIni = ((c.Width - perc) / 2);

            p.Location = new Point((int)ponIni, 0);
            p.Size = new System.Drawing.Size((int)(perc), (int)(nAltura));
            p.Size = new Size((int)(perc), (int)(nAltura));
            p.SizeMode = PictureBoxSizeMode.StretchImage;
        }

        public void loadArranjoUsuario()
        {
            for (int linhas = 0; linhas < int.Parse(edtLinhas.Text); linhas++)
            {
                for (int colunas = 0; colunas < int.Parse(edtColunas.Text); colunas++)
                {
                    //  NovoPanel(linhas, colunas);
                    if (principalController.getElementoArranjo(principalController.dadosArranjo.ID_ARRANJO, linhas, colunas) != null)
                    {

                        var c = (Panel)tableLayoutPanel1.GetControlFromPosition(colunas, linhas);

                        foreach (PictureBox p in c.Controls.OfType<PictureBox>())
                        {
                            p.Dock = DockStyle.Fill;
                            if (principalController.elemento.elemento.ID_IMAGEM > 0) {
                                p.Image = ByteToImage(principalController.getImagem(principalController.elemento.elemento.ID_IMAGEM).img);
                                p.Tag = principalController.getImagem(principalController.elemento.elemento.ID_IMAGEM).ID_IMAGEM;
                                int v = tableLayoutPanel1.Width;
                                int h = tableLayoutPanel1.Height;
                                setPictureImageContent(p, c);
                            }
                        }
                        foreach (TextBox txtBox in c.Controls.OfType<TextBox>())
                        {
                            txtBox.Text = principalController.elemento.elemento.TXT_LABEL;

                        }
                    }
                }
            }
        }

        private void carregarArranjoSelecionado()
        {
            //     panel2.Visible = false;
            if ((listArranjos.SelectedIndex > -1) && (listBoxUsuario.SelectedIndex > -1))
            {
                string user = listBoxUsuario.Items[listBoxUsuario.SelectedIndex].ToString();
                string nome = listArranjos.Items[listArranjos.SelectedIndex].ToString();
                lbNomeArranjo.Text = nome;
                if (principalController.getDadosArranjo(nome, user, idLoginProfessor))
                {
                    edtColunas.Text = principalController.dadosArranjo.Colunas.ToString();
                    edtLinhas.Text = principalController.dadosArranjo.Linhas.ToString();
                    edtNomePrancha.Text = principalController.dadosArranjo.NomeArranjo;
                    tableLayoutPanel1.SuspendLayout();
                    tableLayoutPanel1.Controls.Clear();
                    MontarLayout(int.Parse(edtLinhas.Text), int.Parse(edtColunas.Text));
                    principalController.MontarArranjo(nome, user, idLoginProfessor);
                    for (int linhas = 0; linhas < int.Parse(edtLinhas.Text); linhas++)
                    {
                        for (int colunas = 0; colunas < int.Parse(edtColunas.Text); colunas++)
                        {
                            NovoPanel(linhas, colunas);
                        }
                    }
                    tableLayoutPanel1.ResumeLayout(false);
                    tableLayoutPanel1.PerformLayout();
                    loadArranjoUsuario();
                }
            };

            //  panel2.Visible = true;
            btnGerarArranjo.Enabled = false;
            edtColunas.Enabled = false;
            edtLinhas.Enabled = false;
            edtNomePrancha.Enabled = false;
        }

        private void listArranjos_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                carregarArranjoSelecionado();
            }
            catch
            {
                MessageBox.Show("Falha ao carregar arranjo. Operação cancelada.");
            }
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel1_Click(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel1_MouseClick(object sender, MouseEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            CheckBox ch = new CheckBox();
            PanelCabecalho.Controls.Add(ch);

        }

        public void salvarArranjo()
        {
            principalController.LimparElementosArranjo();
            foreach (Panel c in this.tableLayoutPanel1.Controls.OfType<Panel>())
            {
                string txtLabel = "";
                byte[] img;
                int idImagem = -1;
                foreach (TextBox txtBox in c.Controls.OfType<TextBox>())
                {
                    txtLabel = txtBox.Text;
                }

                foreach (PictureBox picture in c.Controls.OfType<PictureBox>())
                {
                    idImagem = Convert.ToInt32(picture.Tag);
                }
                int linha = -1;
                int coluna = -1;
                foreach (CheckBox check in c.Controls.OfType<CheckBox>())
                {
                    string nome = check.Name;
                    linha = Convert.ToInt32(nome.Substring(0, 4));
                    coluna = Convert.ToInt32(nome.Substring(5, 4));
                }
                principalController.AddElemento(edtNomePrancha.Text, listBoxUsuario.Items[listBoxUsuario.SelectedIndex].ToString(), idLoginProfessor, linha, coluna, txtLabel, idImagem);
            }
        }


        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                salvarArranjo();
                MessageBox.Show("Arranjo gravado com sucesso.");
            }
            catch
            {
                MessageBox.Show("Falha ao gravar o arranjo.");
            }
        }

        private void listArranjos_Click(object sender, EventArgs e)
        {

        }

        private void groupBox6_Resize(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel1_SizeChanged(object sender, EventArgs e)
        {

        }

        private void frmEditor_SizeChanged(object sender, EventArgs e)
        {
        
            if ((this.WindowState != FormWindowState.Maximized) && (this.WindowState != FormWindowState.Minimized))
            {
                this.WindowState = FormWindowState.Maximized;
                this.FormBorderStyle = FormBorderStyle.FixedDialog;

            }
       
        }

        private void btnExcluirUsuario_Click(object sender, EventArgs e)
        {
            try
            {
                principalController.ExluirUsuario(edtExcluirLogin.Text);
                UsuarioController uss = new UsuarioController();
                dataGridView1.DataSource = uss.ListaUsuarios(idLoginProfessor).Tables[0].DefaultView;
                dataGridView1.Columns.RemoveAt(0);
                CarregarListBoxUsuario();
                MessageBox.Show("Usuário removido com sucesso.");
            }
            catch
            {

            }
        }

        private void btnSelImagem_Click(object sender, EventArgs e)
        {

            fileSelLogo.InitialDirectory = "c:";
            fileSelLogo.Filter = "jpg (*.jpg)|*.jpg|jpeg (*.jpeg)|*.jpeg";
            if (fileSelLogo.ShowDialog() == DialogResult.OK)
            {
                logoEscola = File.ReadAllBytes(fileSelLogo.FileName);
                img_Importar.SizeMode = PictureBoxSizeMode.StretchImage;
                img_Importar.Image = ByteToImage(logoEscola);
                Image img;
                img = ByteToImage(logoEscola);
                float perc = principalController.getPercentual(img.Width, img.Height, ((400)));
                float nAltura = principalController.getPercentual(img.Height, img.Width, (perc));
                img_Importar.Dock = DockStyle.None;
                img_Importar.Size = new System.Drawing.Size((int)(perc), (int)(nAltura));
                img_Importar.Size = new Size((int)(perc), (int)(nAltura));
                img_Importar.SizeMode = PictureBoxSizeMode.StretchImage;
                principalController.imgController.img.FILENAME = fileSelLogo.FileName;
                principalController.imgController.img.TEXTO_TO_USER = edtTextToUserImp.Text;
                principalController.imgController.img.img = File.ReadAllBytes(fileSelLogo.FileName);

                grnGravarImagem.Enabled = true;
            }
        }

        private void grnGravarImagem_Click(object sender, EventArgs e)
        {
            try
            {
                principalController.imgController.img.TEXTO_TO_USER = edtTextToUserImp.Text;
                principalController.AddImagem(principalController.imgController.img.FILENAME, edtTextToUserImp.Text, principalController.imgController.img.img);
                grnGravarImagem.Enabled = false;
                MessageBox.Show("Imagem adicionada ao banco de dados com sucesso.");
            }
            catch
            {

                // Console.Write( e.Message);
            }
        }

        private void tbEditor_Click(object sender, EventArgs e)
        {
            tableLayoutPanel1.Hide();
            if (tbEditor.SelectedIndex == 0) {
                carregarArranjoSelecionado();
            }
            tableLayoutPanel1.Show();
        }

        private void frmEditor_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void AbrirMenuAjuda(){

            string appPath = System.IO.Directory.GetCurrentDirectory();
            string helpFileName = appPath + "\\Ajuda.chm";
            if (System.IO.File.Exists(helpFileName))
            {
                Help.ShowHelp(this, helpFileName);
            }

        }

        private void btnAjuda_Click(object sender, EventArgs e)
        {
            AbrirMenuAjuda();
        }

        private void chkIniciais_Click(object sender, EventArgs e)
        {
            chkContem.Checked = !chkIniciais.Checked;
            BuscarPalavras(edtFindWord.Text);
        }

        private void chkContem_Click(object sender, EventArgs e)
        {
            chkIniciais.Checked = !chkContem.Checked;
            BuscarPalavras(edtFindWord.Text);
        }
        
        void NovaThread()
        {
            for (int i = 0; i < 10000; i++) Console.Write("2");
            BuscarPalavras(edtFindWord.Text);
        }

        private void teste() {
            Thread v = new Thread(NovaThread);
            v.Start();
        }

        private DateTime TM = DateTime.Now;
        private DateTime old;


        private void edtFindWord_TextChanged(object sender, EventArgs e)
        {
            BuscarPalavras(edtFindWord.Text);
        }

        private void frmEditor_Load(object sender, EventArgs e)
        {

        }

        private void tbEditor_KeyDown(object sender, KeyEventArgs e)
        {
            if (Keys.F1 == e.KeyCode)
            {
                AbrirMenuAjuda();
            }
        }

        private void cbPrintFormat_KeyPress(object sender, KeyPressEventArgs e)
        {
      
        }

        private void cbPrintFormat_KeyDown(object sender, KeyEventArgs e)
        {
            e.SuppressKeyPress = true;
        }
    }
}
