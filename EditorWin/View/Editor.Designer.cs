﻿namespace EditorWin.View
{
    partial class frmEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEditor));
            this.tbEditor = new System.Windows.Forms.TabControl();
            this.tabEditor = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panelToPrint = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.PanelCabecalho = new System.Windows.Forms.Panel();
            this.lbNomeArranjo = new System.Windows.Forms.Label();
            this.lbNomeEscolaPrint = new System.Windows.Forms.Label();
            this.imgLogoEscolaPrint = new System.Windows.Forms.PictureBox();
            this.blablabla = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.listView1 = new System.Windows.Forms.ListView();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.chkContem = new System.Windows.Forms.CheckBox();
            this.chkIniciais = new System.Windows.Forms.CheckBox();
            this.lbCorresp = new System.Windows.Forms.Label();
            this.edtFindWord = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnAjuda = new System.Windows.Forms.Button();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.btnImprimir = new System.Windows.Forms.Button();
            this.cbPrintFormat = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.btnExcluirArranjo = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.listArranjos = new System.Windows.Forms.ListBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.edtNomePrancha = new System.Windows.Forms.TextBox();
            this.btnGerarArranjo = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.edtColunas = new System.Windows.Forms.TextBox();
            this.edtLinhas = new System.Windows.Forms.TextBox();
            this.btnNovaPrancha = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.listBoxUsuario = new System.Windows.Forms.ListBox();
            this.tbUsuario = new System.Windows.Forms.TabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.panel3 = new System.Windows.Forms.Panel();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.edtExcluirLogin = new System.Windows.Forms.TextBox();
            this.btnExcluirUsuario = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.chProfessor = new System.Windows.Forms.CheckBox();
            this.chAluno = new System.Windows.Forms.CheckBox();
            this.edtLogin = new System.Windows.Forms.TextBox();
            this.edtNome = new System.Windows.Forms.TextBox();
            this.btnGravarUsuario = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tabConfigurações = new System.Windows.Forms.TabPage();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.logoSelecionada = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.btnSelLogo = new System.Windows.Forms.Button();
            this.edtIdentificaEscola = new System.Windows.Forms.TextBox();
            this.chkImprimir = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tbImport = new System.Windows.Forms.TabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.img_Importar = new System.Windows.Forms.PictureBox();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.grnGravarImagem = new System.Windows.Forms.Button();
            this.edtTextToUserImp = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnSelImagem = new System.Windows.Forms.Button();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.fileSelLogo = new System.Windows.Forms.OpenFileDialog();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.tbEditor.SuspendLayout();
            this.tabEditor.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panelToPrint.SuspendLayout();
            this.panel2.SuspendLayout();
            this.PanelCabecalho.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgLogoEscolaPrint)).BeginInit();
            this.groupBox6.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tbUsuario.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel3.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.tabConfigurações.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.logoSelecionada)).BeginInit();
            this.tbImport.SuspendLayout();
            this.panel5.SuspendLayout();
            this.groupBox12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.img_Importar)).BeginInit();
            this.groupBox11.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbEditor
            // 
            this.tbEditor.Controls.Add(this.tabEditor);
            this.tbEditor.Controls.Add(this.tbUsuario);
            this.tbEditor.Controls.Add(this.tabConfigurações);
            this.tbEditor.Controls.Add(this.tbImport);
            this.tbEditor.Controls.Add(this.tabPage1);
            this.tbEditor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbEditor.Location = new System.Drawing.Point(0, 0);
            this.tbEditor.Name = "tbEditor";
            this.tbEditor.SelectedIndex = 0;
            this.tbEditor.Size = new System.Drawing.Size(878, 613);
            this.tbEditor.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.tbEditor.TabIndex = 0;
            this.tbEditor.Click += new System.EventHandler(this.tbEditor_Click);
            this.tbEditor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbEditor_KeyDown);
            // 
            // tabEditor
            // 
            this.tabEditor.Controls.Add(this.panel1);
            this.tabEditor.Controls.Add(this.groupBox1);
            this.tabEditor.Location = new System.Drawing.Point(4, 22);
            this.tabEditor.Name = "tabEditor";
            this.tabEditor.Padding = new System.Windows.Forms.Padding(3);
            this.tabEditor.Size = new System.Drawing.Size(870, 587);
            this.tabEditor.TabIndex = 0;
            this.tabEditor.Text = "Editor";
            this.tabEditor.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panelToPrint);
            this.panel1.Controls.Add(this.groupBox6);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(168, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(699, 581);
            this.panel1.TabIndex = 1;
            // 
            // panelToPrint
            // 
            this.panelToPrint.Controls.Add(this.panel2);
            this.panelToPrint.Controls.Add(this.PanelCabecalho);
            this.panelToPrint.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelToPrint.Location = new System.Drawing.Point(159, 0);
            this.panelToPrint.Name = "panelToPrint";
            this.panelToPrint.Size = new System.Drawing.Size(540, 581);
            this.panelToPrint.TabIndex = 2;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tableLayoutPanel1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 94);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(540, 487);
            this.panel2.TabIndex = 3;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.OutsetDouble;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 37.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 37.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(540, 487);
            this.tableLayoutPanel1.TabIndex = 2;
            this.tableLayoutPanel1.SizeChanged += new System.EventHandler(this.tableLayoutPanel1_SizeChanged);
            this.tableLayoutPanel1.Click += new System.EventHandler(this.tableLayoutPanel1_Click);
            this.tableLayoutPanel1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.tableLayoutPanel1_MouseClick);
            // 
            // PanelCabecalho
            // 
            this.PanelCabecalho.Controls.Add(this.lbNomeArranjo);
            this.PanelCabecalho.Controls.Add(this.lbNomeEscolaPrint);
            this.PanelCabecalho.Controls.Add(this.imgLogoEscolaPrint);
            this.PanelCabecalho.Controls.Add(this.blablabla);
            this.PanelCabecalho.Controls.Add(this.label4);
            this.PanelCabecalho.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelCabecalho.Location = new System.Drawing.Point(0, 0);
            this.PanelCabecalho.Name = "PanelCabecalho";
            this.PanelCabecalho.Size = new System.Drawing.Size(540, 94);
            this.PanelCabecalho.TabIndex = 2;
            // 
            // lbNomeArranjo
            // 
            this.lbNomeArranjo.AutoSize = true;
            this.lbNomeArranjo.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNomeArranjo.Location = new System.Drawing.Point(133, 41);
            this.lbNomeArranjo.Name = "lbNomeArranjo";
            this.lbNomeArranjo.Size = new System.Drawing.Size(112, 16);
            this.lbNomeArranjo.TabIndex = 9;
            this.lbNomeArranjo.Text = "lbNomeArranjo";
            // 
            // lbNomeEscolaPrint
            // 
            this.lbNomeEscolaPrint.AutoSize = true;
            this.lbNomeEscolaPrint.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNomeEscolaPrint.Location = new System.Drawing.Point(151, 16);
            this.lbNomeEscolaPrint.Name = "lbNomeEscolaPrint";
            this.lbNomeEscolaPrint.Size = new System.Drawing.Size(140, 16);
            this.lbNomeEscolaPrint.TabIndex = 8;
            this.lbNomeEscolaPrint.Text = "lbNomeEscolaPrint";
            // 
            // imgLogoEscolaPrint
            // 
            this.imgLogoEscolaPrint.Dock = System.Windows.Forms.DockStyle.Right;
            this.imgLogoEscolaPrint.Location = new System.Drawing.Point(416, 0);
            this.imgLogoEscolaPrint.Name = "imgLogoEscolaPrint";
            this.imgLogoEscolaPrint.Size = new System.Drawing.Size(124, 94);
            this.imgLogoEscolaPrint.TabIndex = 7;
            this.imgLogoEscolaPrint.TabStop = false;
            // 
            // blablabla
            // 
            this.blablabla.AutoSize = true;
            this.blablabla.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.blablabla.Location = new System.Drawing.Point(6, 41);
            this.blablabla.Name = "blablabla";
            this.blablabla.Size = new System.Drawing.Size(125, 16);
            this.blablabla.TabIndex = 1;
            this.blablabla.Text = "Nome da Prancha:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(142, 16);
            this.label4.TabIndex = 0;
            this.label4.Text = "Instituição de Ensino:";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.groupBox9);
            this.groupBox6.Controls.Add(this.groupBox7);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox6.Location = new System.Drawing.Point(0, 0);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(159, 581);
            this.groupBox6.TabIndex = 1;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Busca imagens";
            this.groupBox6.Resize += new System.EventHandler(this.groupBox6_Resize);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.listView1);
            this.groupBox9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox9.Location = new System.Drawing.Point(3, 120);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(153, 458);
            this.groupBox9.TabIndex = 7;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Clique para adicionar a imagem";
            // 
            // listView1
            // 
            this.listView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(3, 16);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(147, 439);
            this.listView1.TabIndex = 3;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.Click += new System.EventHandler(this.listView1_Click);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.chkContem);
            this.groupBox7.Controls.Add(this.chkIniciais);
            this.groupBox7.Controls.Add(this.lbCorresp);
            this.groupBox7.Controls.Add(this.edtFindWord);
            this.groupBox7.Controls.Add(this.label8);
            this.groupBox7.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox7.Location = new System.Drawing.Point(3, 16);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(153, 104);
            this.groupBox7.TabIndex = 5;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Busca palavras";
            // 
            // chkContem
            // 
            this.chkContem.AutoSize = true;
            this.chkContem.Dock = System.Windows.Forms.DockStyle.Top;
            this.chkContem.Location = new System.Drawing.Point(3, 79);
            this.chkContem.Name = "chkContem";
            this.chkContem.Size = new System.Drawing.Size(147, 17);
            this.chkContem.TabIndex = 22;
            this.chkContem.Text = "Contém na palavra";
            this.chkContem.UseVisualStyleBackColor = true;
            this.chkContem.Click += new System.EventHandler(this.chkContem_Click);
            // 
            // chkIniciais
            // 
            this.chkIniciais.AutoSize = true;
            this.chkIniciais.Checked = true;
            this.chkIniciais.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIniciais.Dock = System.Windows.Forms.DockStyle.Top;
            this.chkIniciais.Location = new System.Drawing.Point(3, 62);
            this.chkIniciais.Name = "chkIniciais";
            this.chkIniciais.Size = new System.Drawing.Size(147, 17);
            this.chkIniciais.TabIndex = 21;
            this.chkIniciais.Text = "Iniciais da palavra";
            this.chkIniciais.UseVisualStyleBackColor = true;
            this.chkIniciais.Click += new System.EventHandler(this.chkIniciais_Click);
            // 
            // lbCorresp
            // 
            this.lbCorresp.AutoSize = true;
            this.lbCorresp.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbCorresp.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lbCorresp.Location = new System.Drawing.Point(3, 49);
            this.lbCorresp.Name = "lbCorresp";
            this.lbCorresp.Size = new System.Drawing.Size(0, 13);
            this.lbCorresp.TabIndex = 20;
            // 
            // edtFindWord
            // 
            this.edtFindWord.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.edtFindWord.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.edtFindWord.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edtFindWord.Dock = System.Windows.Forms.DockStyle.Top;
            this.edtFindWord.Location = new System.Drawing.Point(3, 29);
            this.edtFindWord.Name = "edtFindWord";
            this.edtFindWord.Size = new System.Drawing.Size(147, 20);
            this.edtFindWord.TabIndex = 19;
            this.edtFindWord.TextChanged += new System.EventHandler(this.edtFindWord_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Top;
            this.label8.Location = new System.Drawing.Point(3, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(140, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Digite no mínimo duas letras";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnAjuda);
            this.groupBox1.Controls.Add(this.btnSalvar);
            this.groupBox1.Controls.Add(this.btnImprimir);
            this.groupBox1.Controls.Add(this.cbPrintFormat);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.btnExcluirArranjo);
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.btnNovaPrancha);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(165, 581);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // btnAjuda
            // 
            this.btnAjuda.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAjuda.Location = new System.Drawing.Point(3, 509);
            this.btnAjuda.Name = "btnAjuda";
            this.btnAjuda.Size = new System.Drawing.Size(159, 26);
            this.btnAjuda.TabIndex = 26;
            this.btnAjuda.Text = "Ajuda";
            this.btnAjuda.UseVisualStyleBackColor = true;
            this.btnAjuda.Click += new System.EventHandler(this.btnAjuda_Click);
            // 
            // btnSalvar
            // 
            this.btnSalvar.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnSalvar.Location = new System.Drawing.Point(3, 483);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(159, 26);
            this.btnSalvar.TabIndex = 25;
            this.btnSalvar.Text = "Gravar";
            this.btnSalvar.UseVisualStyleBackColor = true;
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // btnImprimir
            // 
            this.btnImprimir.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnImprimir.Location = new System.Drawing.Point(3, 457);
            this.btnImprimir.Name = "btnImprimir";
            this.btnImprimir.Size = new System.Drawing.Size(159, 26);
            this.btnImprimir.TabIndex = 24;
            this.btnImprimir.Text = "Imprimir ";
            this.btnImprimir.UseVisualStyleBackColor = true;
            this.btnImprimir.Click += new System.EventHandler(this.btnImprimir_Click);
            // 
            // cbPrintFormat
            // 
            this.cbPrintFormat.Dock = System.Windows.Forms.DockStyle.Top;
            this.cbPrintFormat.FormattingEnabled = true;
            this.cbPrintFormat.Items.AddRange(new object[] {
            "Paisagem",
            "Retrato"});
            this.cbPrintFormat.Location = new System.Drawing.Point(3, 436);
            this.cbPrintFormat.Name = "cbPrintFormat";
            this.cbPrintFormat.Size = new System.Drawing.Size(159, 21);
            this.cbPrintFormat.Sorted = true;
            this.cbPrintFormat.TabIndex = 23;
            this.cbPrintFormat.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbPrintFormat_KeyDown);
            this.cbPrintFormat.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbPrintFormat_KeyPress);
            // 
            // label12
            // 
            this.label12.Dock = System.Windows.Forms.DockStyle.Top;
            this.label12.Location = new System.Drawing.Point(3, 413);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(159, 23);
            this.label12.TabIndex = 22;
            this.label12.Text = "Selecione o formato da folha";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnExcluirArranjo
            // 
            this.btnExcluirArranjo.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnExcluirArranjo.Location = new System.Drawing.Point(3, 387);
            this.btnExcluirArranjo.Name = "btnExcluirArranjo";
            this.btnExcluirArranjo.Size = new System.Drawing.Size(159, 26);
            this.btnExcluirArranjo.TabIndex = 12;
            this.btnExcluirArranjo.Text = "Excluir Prancha";
            this.btnExcluirArranjo.UseVisualStyleBackColor = true;
            this.btnExcluirArranjo.Click += new System.EventHandler(this.btnExcluirArranjo_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.listArranjos);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox4.Location = new System.Drawing.Point(3, 282);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(159, 105);
            this.groupBox4.TabIndex = 9;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Lista Pranchas do Usuário";
            // 
            // listArranjos
            // 
            this.listArranjos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listArranjos.FormattingEnabled = true;
            this.listArranjos.Location = new System.Drawing.Point(3, 16);
            this.listArranjos.Name = "listArranjos";
            this.listArranjos.Size = new System.Drawing.Size(153, 86);
            this.listArranjos.TabIndex = 0;
            this.listArranjos.SelectedIndexChanged += new System.EventHandler(this.listArranjos_SelectedIndexChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.edtNomePrancha);
            this.groupBox3.Controls.Add(this.btnGerarArranjo);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.edtColunas);
            this.groupBox3.Controls.Add(this.edtLinhas);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox3.Location = new System.Drawing.Point(3, 156);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(159, 126);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Nova Prancha";
            // 
            // edtNomePrancha
            // 
            this.edtNomePrancha.Enabled = false;
            this.edtNomePrancha.Location = new System.Drawing.Point(3, 15);
            this.edtNomePrancha.MaxLength = 100;
            this.edtNomePrancha.Name = "edtNomePrancha";
            this.edtNomePrancha.Size = new System.Drawing.Size(156, 20);
            this.edtNomePrancha.TabIndex = 13;
            // 
            // btnGerarArranjo
            // 
            this.btnGerarArranjo.Enabled = false;
            this.btnGerarArranjo.Location = new System.Drawing.Point(5, 93);
            this.btnGerarArranjo.Name = "btnGerarArranjo";
            this.btnGerarArranjo.Size = new System.Drawing.Size(151, 26);
            this.btnGerarArranjo.TabIndex = 12;
            this.btnGerarArranjo.Text = "Gerar Prancha";
            this.btnGerarArranjo.UseVisualStyleBackColor = true;
            this.btnGerarArranjo.Click += new System.EventHandler(this.btnGerarArranjo_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(2, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Colunas";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Linhas";
            // 
            // edtColunas
            // 
            this.edtColunas.Enabled = false;
            this.edtColunas.Location = new System.Drawing.Point(59, 67);
            this.edtColunas.MaxLength = 2;
            this.edtColunas.Name = "edtColunas";
            this.edtColunas.Size = new System.Drawing.Size(100, 20);
            this.edtColunas.TabIndex = 9;
            this.edtColunas.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.edtColunas_KeyPress);
            // 
            // edtLinhas
            // 
            this.edtLinhas.Enabled = false;
            this.edtLinhas.Location = new System.Drawing.Point(59, 41);
            this.edtLinhas.MaxLength = 2;
            this.edtLinhas.Name = "edtLinhas";
            this.edtLinhas.Size = new System.Drawing.Size(100, 20);
            this.edtLinhas.TabIndex = 8;
            this.edtLinhas.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.edtLinhas_KeyPress);
            // 
            // btnNovaPrancha
            // 
            this.btnNovaPrancha.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnNovaPrancha.Location = new System.Drawing.Point(3, 130);
            this.btnNovaPrancha.Name = "btnNovaPrancha";
            this.btnNovaPrancha.Size = new System.Drawing.Size(159, 26);
            this.btnNovaPrancha.TabIndex = 1;
            this.btnNovaPrancha.Text = "Nova Prancha";
            this.btnNovaPrancha.UseVisualStyleBackColor = true;
            this.btnNovaPrancha.Click += new System.EventHandler(this.btnNovaPrancha_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.listBoxUsuario);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(3, 16);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(159, 114);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Lista Usuarios";
            // 
            // listBoxUsuario
            // 
            this.listBoxUsuario.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxUsuario.FormattingEnabled = true;
            this.listBoxUsuario.Location = new System.Drawing.Point(3, 16);
            this.listBoxUsuario.Name = "listBoxUsuario";
            this.listBoxUsuario.Size = new System.Drawing.Size(153, 95);
            this.listBoxUsuario.TabIndex = 0;
            this.listBoxUsuario.Click += new System.EventHandler(this.listBoxUsuario_Click);
            // 
            // tbUsuario
            // 
            this.tbUsuario.Controls.Add(this.panel4);
            this.tbUsuario.Controls.Add(this.panel3);
            this.tbUsuario.Location = new System.Drawing.Point(4, 22);
            this.tbUsuario.Name = "tbUsuario";
            this.tbUsuario.Padding = new System.Windows.Forms.Padding(3);
            this.tbUsuario.Size = new System.Drawing.Size(870, 587);
            this.tbUsuario.TabIndex = 1;
            this.tbUsuario.Text = "Usuários";
            this.tbUsuario.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.dataGridView1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(3, 152);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(864, 432);
            this.panel4.TabIndex = 1;
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Top;
            this.dataGridView1.Enabled = false;
            this.dataGridView1.GridColor = System.Drawing.SystemColors.Window;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(864, 195);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.groupBox10);
            this.panel3.Controls.Add(this.groupBox5);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(3, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(864, 149);
            this.panel3.TabIndex = 0;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.edtExcluirLogin);
            this.groupBox10.Controls.Add(this.btnExcluirUsuario);
            this.groupBox10.Controls.Add(this.label7);
            this.groupBox10.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox10.Location = new System.Drawing.Point(275, 0);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(275, 149);
            this.groupBox10.TabIndex = 2;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Excluir usuário";
            // 
            // edtExcluirLogin
            // 
            this.edtExcluirLogin.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edtExcluirLogin.Location = new System.Drawing.Point(58, 19);
            this.edtExcluirLogin.Name = "edtExcluirLogin";
            this.edtExcluirLogin.Size = new System.Drawing.Size(201, 20);
            this.edtExcluirLogin.TabIndex = 5;
            // 
            // btnExcluirUsuario
            // 
            this.btnExcluirUsuario.Location = new System.Drawing.Point(58, 46);
            this.btnExcluirUsuario.Name = "btnExcluirUsuario";
            this.btnExcluirUsuario.Size = new System.Drawing.Size(201, 23);
            this.btnExcluirUsuario.TabIndex = 3;
            this.btnExcluirUsuario.Text = "Excluir usuário";
            this.btnExcluirUsuario.UseVisualStyleBackColor = true;
            this.btnExcluirUsuario.Click += new System.EventHandler(this.btnExcluirUsuario_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(23, 27);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Login:";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.chProfessor);
            this.groupBox5.Controls.Add(this.chAluno);
            this.groupBox5.Controls.Add(this.edtLogin);
            this.groupBox5.Controls.Add(this.edtNome);
            this.groupBox5.Controls.Add(this.btnGravarUsuario);
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Controls.Add(this.label3);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox5.Location = new System.Drawing.Point(0, 0);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(275, 149);
            this.groupBox5.TabIndex = 1;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Dados usuário";
            // 
            // chProfessor
            // 
            this.chProfessor.AutoSize = true;
            this.chProfessor.Location = new System.Drawing.Point(148, 74);
            this.chProfessor.Name = "chProfessor";
            this.chProfessor.Size = new System.Drawing.Size(70, 17);
            this.chProfessor.TabIndex = 7;
            this.chProfessor.Text = "Professor";
            this.chProfessor.UseVisualStyleBackColor = true;
            this.chProfessor.CheckedChanged += new System.EventHandler(this.chProfessor_CheckedChanged);
            // 
            // chAluno
            // 
            this.chAluno.AutoSize = true;
            this.chAluno.Checked = true;
            this.chAluno.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chAluno.Location = new System.Drawing.Point(58, 74);
            this.chAluno.Name = "chAluno";
            this.chAluno.Size = new System.Drawing.Size(53, 17);
            this.chAluno.TabIndex = 6;
            this.chAluno.Text = "Aluno";
            this.chAluno.UseVisualStyleBackColor = true;
            this.chAluno.Click += new System.EventHandler(this.chAluno_Click);
            // 
            // edtLogin
            // 
            this.edtLogin.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edtLogin.Location = new System.Drawing.Point(58, 48);
            this.edtLogin.Name = "edtLogin";
            this.edtLogin.Size = new System.Drawing.Size(201, 20);
            this.edtLogin.TabIndex = 5;
            // 
            // edtNome
            // 
            this.edtNome.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edtNome.Location = new System.Drawing.Point(58, 19);
            this.edtNome.Name = "edtNome";
            this.edtNome.Size = new System.Drawing.Size(201, 20);
            this.edtNome.TabIndex = 4;
            // 
            // btnGravarUsuario
            // 
            this.btnGravarUsuario.Location = new System.Drawing.Point(58, 97);
            this.btnGravarUsuario.Name = "btnGravarUsuario";
            this.btnGravarUsuario.Size = new System.Drawing.Size(130, 23);
            this.btnGravarUsuario.TabIndex = 3;
            this.btnGravarUsuario.Text = "Gravar Usuário";
            this.btnGravarUsuario.UseVisualStyleBackColor = true;
            this.btnGravarUsuario.Click += new System.EventHandler(this.btnGravarUsuario_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(23, 27);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Nome:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Login:";
            // 
            // tabConfigurações
            // 
            this.tabConfigurações.Controls.Add(this.panel7);
            this.tabConfigurações.Controls.Add(this.panel6);
            this.tabConfigurações.Location = new System.Drawing.Point(4, 22);
            this.tabConfigurações.Name = "tabConfigurações";
            this.tabConfigurações.Size = new System.Drawing.Size(870, 587);
            this.tabConfigurações.TabIndex = 2;
            this.tabConfigurações.Text = "Configurações";
            this.tabConfigurações.UseVisualStyleBackColor = true;
            // 
            // panel7
            // 
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 115);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(870, 181);
            this.panel7.TabIndex = 1;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.logoSelecionada);
            this.panel6.Controls.Add(this.button1);
            this.panel6.Controls.Add(this.btnSelLogo);
            this.panel6.Controls.Add(this.edtIdentificaEscola);
            this.panel6.Controls.Add(this.chkImprimir);
            this.panel6.Controls.Add(this.label10);
            this.panel6.Controls.Add(this.label9);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(870, 115);
            this.panel6.TabIndex = 0;
            // 
            // logoSelecionada
            // 
            this.logoSelecionada.Location = new System.Drawing.Point(672, 8);
            this.logoSelecionada.Name = "logoSelecionada";
            this.logoSelecionada.Size = new System.Drawing.Size(139, 101);
            this.logoSelecionada.TabIndex = 6;
            this.logoSelecionada.TabStop = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(8, 89);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(122, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "Salvar Configuração";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnSelLogo
            // 
            this.btnSelLogo.Location = new System.Drawing.Point(126, 30);
            this.btnSelLogo.Name = "btnSelLogo";
            this.btnSelLogo.Size = new System.Drawing.Size(142, 23);
            this.btnSelLogo.TabIndex = 4;
            this.btnSelLogo.Text = "Selecionar Logomarca";
            this.btnSelLogo.UseVisualStyleBackColor = true;
            this.btnSelLogo.Click += new System.EventHandler(this.btnSelLogo_Click);
            // 
            // edtIdentificaEscola
            // 
            this.edtIdentificaEscola.Location = new System.Drawing.Point(184, 8);
            this.edtIdentificaEscola.Name = "edtIdentificaEscola";
            this.edtIdentificaEscola.Size = new System.Drawing.Size(482, 20);
            this.edtIdentificaEscola.TabIndex = 3;
            // 
            // chkImprimir
            // 
            this.chkImprimir.AutoSize = true;
            this.chkImprimir.Checked = true;
            this.chkImprimir.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkImprimir.Location = new System.Drawing.Point(8, 66);
            this.chkImprimir.Name = "chkImprimir";
            this.chkImprimir.Size = new System.Drawing.Size(202, 17);
            this.chkImprimir.TabIndex = 2;
            this.chkImprimir.Text = "Imprimir Identificação na PRANCHAS";
            this.chkImprimir.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(8, 40);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(112, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "Selecionar logomarca:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 11);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(170, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Identificação do Centro de Ensino:";
            // 
            // tbImport
            // 
            this.tbImport.Controls.Add(this.panel5);
            this.tbImport.Location = new System.Drawing.Point(4, 22);
            this.tbImport.Name = "tbImport";
            this.tbImport.Padding = new System.Windows.Forms.Padding(3);
            this.tbImport.Size = new System.Drawing.Size(870, 587);
            this.tbImport.TabIndex = 3;
            this.tbImport.Text = "Importar Imagens";
            this.tbImport.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.groupBox12);
            this.panel5.Controls.Add(this.groupBox11);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(3, 3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(864, 526);
            this.panel5.TabIndex = 0;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.img_Importar);
            this.groupBox12.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox12.Location = new System.Drawing.Point(229, 0);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(622, 526);
            this.groupBox12.TabIndex = 1;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Imagem";
            // 
            // img_Importar
            // 
            this.img_Importar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.img_Importar.Location = new System.Drawing.Point(3, 16);
            this.img_Importar.Name = "img_Importar";
            this.img_Importar.Size = new System.Drawing.Size(616, 507);
            this.img_Importar.TabIndex = 0;
            this.img_Importar.TabStop = false;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.grnGravarImagem);
            this.groupBox11.Controls.Add(this.edtTextToUserImp);
            this.groupBox11.Controls.Add(this.label6);
            this.groupBox11.Controls.Add(this.btnSelImagem);
            this.groupBox11.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox11.Location = new System.Drawing.Point(0, 0);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(229, 526);
            this.groupBox11.TabIndex = 0;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Selecione a imagem";
            // 
            // grnGravarImagem
            // 
            this.grnGravarImagem.Enabled = false;
            this.grnGravarImagem.Location = new System.Drawing.Point(6, 96);
            this.grnGravarImagem.Name = "grnGravarImagem";
            this.grnGravarImagem.Size = new System.Drawing.Size(217, 23);
            this.grnGravarImagem.TabIndex = 3;
            this.grnGravarImagem.Text = "Gravar imagem";
            this.grnGravarImagem.UseVisualStyleBackColor = true;
            this.grnGravarImagem.Click += new System.EventHandler(this.grnGravarImagem_Click);
            // 
            // edtTextToUserImp
            // 
            this.edtTextToUserImp.Location = new System.Drawing.Point(6, 70);
            this.edtTextToUserImp.Name = "edtTextToUserImp";
            this.edtTextToUserImp.Size = new System.Drawing.Size(217, 20);
            this.edtTextToUserImp.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 54);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Texto para usuário";
            // 
            // btnSelImagem
            // 
            this.btnSelImagem.Location = new System.Drawing.Point(6, 19);
            this.btnSelImagem.Name = "btnSelImagem";
            this.btnSelImagem.Size = new System.Drawing.Size(217, 23);
            this.btnSelImagem.TabIndex = 0;
            this.btnSelImagem.Text = "Selecionar Imagem";
            this.btnSelImagem.UseVisualStyleBackColor = true;
            this.btnSelImagem.Click += new System.EventHandler(this.btnSelImagem_Click);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.textBox1);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(870, 587);
            this.tabPage1.TabIndex = 4;
            this.tabPage1.Text = "Sobre";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox1.Location = new System.Drawing.Point(3, 16);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(864, 568);
            this.textBox1.TabIndex = 2;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Dock = System.Windows.Forms.DockStyle.Top;
            this.label11.Location = new System.Drawing.Point(3, 3);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(128, 13);
            this.label11.TabIndex = 1;
            this.label11.Text = "Autores e direitos autorais";
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // frmEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(878, 613);
            this.Controls.Add(this.tbEditor);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "frmEditor";
            this.Text = "Editor Livre de Prancha de Comunicação";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmEditor_FormClosed);
            this.Load += new System.EventHandler(this.frmEditor_Load);
            this.Shown += new System.EventHandler(this.frmEditor_Shown);
            this.SizeChanged += new System.EventHandler(this.frmEditor_SizeChanged);
            this.tbEditor.ResumeLayout(false);
            this.tabEditor.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panelToPrint.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.PanelCabecalho.ResumeLayout(false);
            this.PanelCabecalho.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgLogoEscolaPrint)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.tbUsuario.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel3.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.tabConfigurações.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.logoSelecionada)).EndInit();
            this.tbImport.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.groupBox12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.img_Importar)).EndInit();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tbEditor;
        private System.Windows.Forms.TabPage tabEditor;
        private System.Windows.Forms.TabPage tbUsuario;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnGerarArranjo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox edtColunas;
        private System.Windows.Forms.TextBox edtLinhas;
        private System.Windows.Forms.Button btnNovaPrancha;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListBox listArranjos;
        private System.Windows.Forms.ListBox listBoxUsuario;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.CheckBox chProfessor;
        private System.Windows.Forms.CheckBox chAluno;
        private System.Windows.Forms.TextBox edtLogin;
        private System.Windows.Forms.TextBox edtNome;
        private System.Windows.Forms.Button btnGravarUsuario;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.Panel panelToPrint;
        private System.Windows.Forms.Panel PanelCabecalho;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.Label blablabla;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabPage tabConfigurações;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.CheckBox chkImprimir;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnSelLogo;
        private System.Windows.Forms.TextBox edtIdentificaEscola;
        private System.Windows.Forms.OpenFileDialog fileSelLogo;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox logoSelecionada;
        private System.Windows.Forms.Label lbNomeEscolaPrint;
        private System.Windows.Forms.PictureBox imgLogoEscolaPrint;
        private System.Windows.Forms.Label lbNomeArranjo;
        private System.Windows.Forms.Button btnExcluirArranjo;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.TextBox edtExcluirLogin;
        private System.Windows.Forms.Button btnExcluirUsuario;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TabPage tbImport;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.PictureBox img_Importar;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Button btnSelImagem;
        private System.Windows.Forms.Button grnGravarImagem;
        private System.Windows.Forms.TextBox edtTextToUserImp;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox edtNomePrancha;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox chkContem;
        private System.Windows.Forms.CheckBox chkIniciais;
        private System.Windows.Forms.Label lbCorresp;
        private System.Windows.Forms.TextBox edtFindWord;
        private System.Windows.Forms.PrintDialog printDialog1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnAjuda;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.Button btnImprimir;
        private System.Windows.Forms.ComboBox cbPrintFormat;
        private System.Windows.Forms.Label label12;
    }
}