﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EditorWin.View;
using EditorWin.Controller;
using System.Diagnostics;

namespace EditorWin
{
    
    public partial class frmLogin : Form
    {
        private PrincipalController principalController;
        public frmEditor editor;
        public frmLogin()
        {
            InitializeComponent();
            principalController = new PrincipalController();
        }

        public void setfrm(frmEditor f)
        {

            editor = f;
        }
        private void Form1_Click(object sender, EventArgs e)
        {

        }

        private void efetuarLogin()
        {
            if (principalController.ValidarLogin(edtLogin.Text))
            {
                if (principalController.login.IsFisrtAccess(edtLogin.Text))
                {
                    var g = DateTime.Now.Day.ToString();
                    if (DateTime.Now.Day.ToString().Length == 1)
                        g = "0" + DateTime.Now.Day.ToString();
                    var b = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() +  g ;

                    MessageBox.Show("Obrigado por utilizar o Editor de Prancha de Comunicação Amplisoft. No primeiro acesso ao software será exibida uma página WEB contendo algumas perguntas que nos ajudarão a melhorar este software. Contamos com sua participação.");
                    Process mypr;
                    System.Diagnostics.Process.Start("https://goo.gl/forms/y3bpej1DrXCdhSf42");
                };

                editor = new frmEditor();
                editor.setProfessor(principalController.getParametrosProfessor(edtLogin.Text).ID_PROFESSOR);
                editor.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Login não encontrado. Operação cancelada.");
            }
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            efetuarLogin();
        }

        private void frmLogin_Shown(object sender, EventArgs e)
        {
           // edtLogin.Text = "admin";
           // efetuarLogin();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
