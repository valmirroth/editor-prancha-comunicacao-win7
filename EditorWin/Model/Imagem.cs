﻿using SQLite.Net.Attributes;
using System;

namespace EditorWin.Controller
{
  class IMAGEM
    {

        [SQLite.PrimaryKey, SQLite.AutoIncrement]
        public int ID_IMAGEM
        {
            get;
            set;
        }
        public String FILENAME
        {
            get;
            set;
        }
        public string TEXTO_TO_USER
        {
            get;
            set;
        }
        public string CUSTOM
        {
            get;
            set;
        }

        public byte[] IMAGE
        {
            get;
            set;
        }

        public byte[] img
        {
            get;
            set;
        }
    }
}
