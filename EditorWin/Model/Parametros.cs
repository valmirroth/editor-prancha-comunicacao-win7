﻿using SQLite.Net.Attributes;
using System;


namespace EditorWin.Model
{
    [Table("Parametros")]
    class Parametros
    {
        [SQLite.PrimaryKey, SQLite.AutoIncrement]
        public int ID_PARAMETROS
        {
            get;
            set;
        }
       
        public int ID_PROFESSOR
        {
            get;
            set;
        }

        public string TEXTO_TO_USER
        {
            get;
            set;
        }

        public bool Imprimir
        {
            get;
            set;
        }

        public byte[] IMAGE
        {
            get;
            set;
        }
    }
}
