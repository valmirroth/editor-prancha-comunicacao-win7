﻿using SQLite.Net.Attributes;
using System;

namespace EditorWin.Model
{
    class Arranjo
    {
        [SQLite.PrimaryKey, SQLite.AutoIncrement]
        public int ID_ARRANJO
        {
            get;
            set;
        }

        public int ID_USUARIO
        {
            get;
            set;
        }

        public int ID_PROFESSOR
        {
            get;
            set;
        }

        public string NomeArranjo
        {
            get;
            set;
        }
        public int Linhas
        {
            get;
            set;
        }
        public int Colunas
        {
            get;
            set;
        }
        public int PosColuna
        {
            get;
            set;
        }

        public int PosLinha
        {
            get;
            set;
        }
    }
}
