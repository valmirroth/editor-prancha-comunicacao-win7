﻿using SQLite.Net.Attributes;
using System;

namespace EditorWin.Model
{
    class Elemento
    {
        [SQLite.PrimaryKey, SQLite.AutoIncrement]
        public int ID_ELEMENTO
        {
            get;
            set;
        }
        public int ID_ARRANJO
        {
            get;
            set;
        }
        public int ID_IMAGEM
        {
            get;
            set;
        }
        public int Linha
        {
            get;
            set;
        }
        public int Coluna
        {
            get;
            set;
        }
        public string TXT_LABEL
        {
            get;
            set;
        }


    }
}
