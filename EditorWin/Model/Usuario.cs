﻿using SQLite.Net.Attributes;
using System;


namespace EditorWin.Controller
{
  class Usuario
    {

        [SQLite.PrimaryKey, SQLite.AutoIncrement]
        public int ID_USUARIO
        {
            get;
            set;
        }
        public int ID_PROFESSOR
        {
            get;
            set;
        }
        public string Nome
        {
            get;
            set;
        }
        public string Perfil
        {
            get;
            set;
        }
        public string Login
        {
            get;
            set;
        }

        public string PrimeiroAcesso
        {
            get;
            set;
        }
        
    }
}
