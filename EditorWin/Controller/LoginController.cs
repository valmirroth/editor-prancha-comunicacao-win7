﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EditorWin.Model;

namespace EditorWin.Controller
{
    class LoginController
    {
        private rnBaseController rnBase;

        public LoginController()
        {
            rnBase = new rnBaseController();
            CriarUsuarioAdmin();
        }

        private void CriarUsuarioAdmin() {
            Usuario us = new Usuario();
            us.Login = "ADMIN";
            us.Nome = "ADMIN";
            us.Perfil = "Professor";
            var rst = rnBase.con.Table<Usuario>().Where(x => x.Login == us.Login).ToList<Usuario>();
            if (rst.Count() == 0) {
                rnBase.con.Insert(us);
                Parametros pr = new Parametros();
                rst.Clear();
                rst = rnBase.con.Table<Usuario>().Where(x => x.Login == us.Login).ToList<Usuario>();
                pr.ID_PROFESSOR = rst[0].ID_USUARIO;
                pr.TEXTO_TO_USER = "Administrador do sistema.";
                rnBase.con.Insert(pr);

            }
        }


        public bool ValidarLogin(string login)
        {
            return rnBase.EfetuarLogin(login);
        }

        public bool IsFisrtAccess(string login)
        {
            return rnBase.IsFirstAccess(login);
        }

        public Parametros getParametrosProfessor(string login)
        {
            var rs = rnBase.con.Table<Usuario>().FirstOrDefault(x => x.Login == login);
            var pr = rnBase.con.Table<Parametros>().FirstOrDefault(x => x.ID_PROFESSOR == rs.ID_USUARIO );
            return pr;    
        }
        public Parametros getParametrosProfessor(int id)
        {
            var rs = rnBase.con.Table<Usuario>().FirstOrDefault(x => x.ID_USUARIO == id);
            var pr = rnBase.con.Table<Parametros>().FirstOrDefault(x => x.ID_PROFESSOR == rs.ID_USUARIO);
            return pr;
        }

    }
}
