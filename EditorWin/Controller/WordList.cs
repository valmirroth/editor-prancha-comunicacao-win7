﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Data;
namespace EditorWin.Controller
{
    class WordList
    {
        private List<IMAGEM> Imagens;
        private rnBaseController rnBase;
        private DataSet dsImagem;
        public TipoBusca tipobusca;
        const string filename = @"C:\Mestrado\Db Imagens\db_Imagens.db3";
        public WordList()
        {
            rnBase = new  rnBaseController();
            dsImagem = new DataSet();
            Imagens = new List<IMAGEM>();
        }

        public void GetImagens(string el)
        {
            var g = Task.Run(() => {
                SQLite.TableQuery<IMAGEM> result;
                if (el.Length <= 1)
                {
                    result = rnBase.con.Table<IMAGEM>().Where(IM => IM.TEXTO_TO_USER == (el)).OrderBy(x => x.TEXTO_TO_USER);
                }
                else
                {
                    if (tipobusca == TipoBusca.Contains)
                    {
                        result = rnBase.con.Table<IMAGEM>().Where(IM => IM.TEXTO_TO_USER.Contains(el)).OrderBy(x => x.TEXTO_TO_USER);
                    }
                    else
                    {
                        result = rnBase.con.Table<IMAGEM>().Where(IM => IM.TEXTO_TO_USER.StartsWith(el)).OrderBy(x => x.TEXTO_TO_USER);
                    }
                }
                Imagens = new List<IMAGEM>();
                foreach (IMAGEM im in result)
                {
                    Imagens.Add(im);
                }

            });
            g.Wait();
        }

        public List<IMAGEM> getWordList(string elemento)
        {
            lock (Imagens) ;
            var g = Task.Run(() =>
            {
                GetImagens(elemento);
            });
            g.Wait();
                        
            return Imagens;
        }

        public IMAGEM getImagem(int idImagem)
        {
            var result = rnBase.con.Table<IMAGEM>().Where(IM => IM.ID_IMAGEM == idImagem).ToList<IMAGEM>()[0];
            return result;
        }

        public List<string> getWordsAutoComplete()
        {

            var result = rnBase.con.Table<IMAGEM>().ToList<IMAGEM>().OrderBy(x => x.TEXTO_TO_USER);
            List<string> lst = new List<string>();
            foreach(IMAGEM sim in result)
            {
                lst.Add(sim.TEXTO_TO_USER);
            }
            return lst;
        }
    }
}
