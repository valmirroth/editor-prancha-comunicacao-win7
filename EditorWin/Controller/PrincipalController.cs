﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EditorWin.Model;
using System.Data;

namespace EditorWin.Controller
{
    enum TipoBusca { StartWith, Contains }
    class PrincipalController
    {
        public ParametrosController parametros;
        public LoginController login;
        public UsuarioController usuario;
        public rnBaseController rnBase;
        public ArranjoController arranjo;
        public ImagemController imgController;
        public Arranjo dadosArranjo;
        public WordList Words;
        public WordList wordlist;
        public List<IMAGEM> imgList;
        public int posLinha = -1 ;
        public int posColuna = -1;

        public ElementoController elemento;

        public TipoBusca tipoBusca;

        public PrincipalController()
        {
            rnBase = new rnBaseController();
            login = new LoginController();
            arranjo = new ArranjoController();
            Words = new WordList();
            parametros = new ParametrosController();
            elemento = new ElementoController();
            imgController = new ImagemController();
            dadosArranjo = new Arranjo();
            wordlist = new WordList();
            imgList = new List<IMAGEM>();
        }

        public float getPercentual(float largura, float altura, float novaAltura)
        {
            float perc = 0;
                perc = ((largura / altura) * novaAltura);
            return perc;
        }

        public Parametros getParametrosProfessor(string sLogin)
        {
            return login.getParametrosProfessor(sLogin);
        }

        public Parametros getParametrosProfessor(int id)
        {
            parametros.userParametros = login.getParametrosProfessor(id);
            return parametros.userParametros;
        }

        public bool ValidarLogin(string sLogin)
        {
            return (login.ValidarLogin(sLogin));
        }

        public bool IsFirstAccess(string sLogin)
        {
            return (login.ValidarLogin(sLogin));
        }

        public void InserirParametro(string sEscola, byte[] logo, bool imprime, int idLoginProfessor)
        {
            Parametros pr = new Parametros();
            pr.ID_PROFESSOR = idLoginProfessor;
            pr.TEXTO_TO_USER = sEscola;
            pr.IMAGE = logo;
            pr.Imprimir = imprime;
            parametros.InserirParametro(pr);
        }

        public string InserirUsuario(string Nome, string Login, bool Professor, int IDPROFESSOR)
        {
            if (Nome == "")
            {
                return "Nome do usuário não esta preenchido.";
            }

            if (Login == "")
            {
                return "Login do usuário não esta preenchido.";
            }
            string Perfil = "Aluno";
            if (Professor)
                Perfil = "Professor";
            Usuario user = new Usuario();
            user.Login = Login;
            user.Nome = Nome;
            user.Perfil = Perfil;
            user.ID_PROFESSOR = IDPROFESSOR;
            usuario = new UsuarioController();
            string rs = usuario.InserirUsuario(user);
            return rs;
        }

        public DataSet getListaUsuario(int idprofessor)
        {
            usuario = new UsuarioController();
            return usuario.ListaUsuarios(idprofessor);
        }

        public List<Arranjo> getArranjos(string sLogin)
        {
            int id = usuario.getIdUsuario(sLogin);
            return arranjo.getArranjos(id);
        }
        public bool ValidarArranjo(string Nome, string slogin, int idProfessor, int linhas, int colunas)
        {
            if ((linhas < 1 )||(colunas < 1) || (Nome == ""))
            {
                return false;
            }
            Arranjo ar = new Arranjo();
            ar.NomeArranjo = Nome;
            ar.ID_PROFESSOR = idProfessor;
            ar.ID_USUARIO = usuario.getIdUsuario(slogin);
            ar.Linhas = linhas;
            ar.Colunas = colunas;
            return arranjo.getExistsArranjo(ar);
        }

        public bool AddArranjo(string Nome, string slogin, int idProfessor, int linhas, int colunas)
        {
            Arranjo ar = new Arranjo();
            ar.NomeArranjo = Nome;
            ar.ID_PROFESSOR = idProfessor;
            ar.ID_USUARIO = usuario.getIdUsuario(slogin);
            ar.Linhas = linhas;
            ar.Colunas = colunas;
            arranjo.AddArranjo(ar);
            return getDadosArranjo(Nome, slogin, idProfessor);
        }

        public bool ExcluirArranjo(string Nome, string slogin, int idProfessor)
        {
            Arranjo ar = new Arranjo();
            ar.NomeArranjo = Nome;
            ar.ID_PROFESSOR = idProfessor;
            ar.ID_USUARIO = usuario.getIdUsuario(slogin);
            return arranjo.RemoveArranjo(ar);
        }

        public bool getDadosArranjo(string Nome, string slogin, int idProfessor)
        {
            Arranjo ar = new Arranjo();
            ar.NomeArranjo = Nome;
            ar.ID_PROFESSOR = idProfessor;
            ar.ID_USUARIO = usuario.getIdUsuario(slogin);
            dadosArranjo = arranjo.getDadosArranjo(ar);
            if (dadosArranjo == null)
            {
                return false;
            }else
            {
                return true;
            }
        }

        public void getListPalavras(string palavra, TipoBusca tp)
        {
            lock (imgList) ;
            var g = Task.Run(() => { 
            imgList.Clear();
            wordlist.tipobusca = tp;
            imgList = wordlist.getWordList(palavra);
            });
            g.Wait();
        }

        public IMAGEM getImagem(string palavra)
        {
           return imgList.FirstOrDefault(x => x.TEXTO_TO_USER == palavra);
        }

        public IMAGEM getImagem(int idImagem)
        {
            return wordlist.getImagem(idImagem);
        }

        public void LimparElementosArranjo()
        {
            elemento.RemoveElementos(dadosArranjo);
        }
        public void AddElemento(string NomeArranjo, string loginUsuario, int idProfessor, int linha, int coluna, string txtUsuario, int idImg)
        {
            Elemento ele = new Elemento();
            ele.Linha = linha;
            ele.Coluna = coluna;
            ele.ID_ARRANJO = dadosArranjo.ID_ARRANJO;
            ele.TXT_LABEL = txtUsuario;
            ele.ID_IMAGEM = idImg;
            elemento.AddElemento(ele);
        }

        public void MontarArranjo(string NomeArranjo, string loginUsuario, int idProfessor)
        {
            Arranjo ar = new Arranjo();
            ar.NomeArranjo = NomeArranjo;
            ar.ID_PROFESSOR = idProfessor;
            ar.ID_USUARIO = usuario.getIdUsuario(loginUsuario);
            dadosArranjo = arranjo.getDadosArranjo(ar);
            elemento.listElemento = elemento.getElementosArranjo(dadosArranjo);
        }

        public Elemento getElementoArranjo(int idArranjo, int linha, int coluna)
        {
           return elemento.getElementoArranjo(idArranjo, linha, coluna);
        }

        public void ExluirUsuario(string sLogin)
        {
            int id = usuario.getIdUsuario(sLogin);
            arranjo.RemoveArranjo(id);
            usuario.RemoverUsuario(id);

        }

        public void AddImagem(string fileName, string txtUser, byte[] img)
        {
            IMAGEM im = new IMAGEM();
            im.FILENAME = fileName;
            im.TEXTO_TO_USER = txtUser;
            im.img = img;
            imgController.AddImagem(im);
        }

    }
}
