﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EditorWin.Model;

namespace EditorWin.Controller
{
    class ElementoController
    {
        private rnBaseController rnBase;
        public Elemento elemento;
        public List<Elemento> listElemento;
        
        public ElementoController()
        {
            rnBase = new rnBaseController();
            listElemento = new List<Elemento>();
            elemento = new Elemento();
        }

        public void AddElemento(Elemento el)
        {
            rnBase.con.Insert(el);
        }

        public bool RemoveElementos(Arranjo ar)
        {
            string sql = "DELETE FROM Elemento WHERE ID_ARRANJO = " + ar.ID_ARRANJO + "";
            rnBase.con.Execute(sql);
            return true;
        }


        public List<Elemento> getElementosArranjo(Arranjo ar)
        {
            var rs = rnBase.con.Table<Elemento>().Where(x => x.ID_ARRANJO == ar.ID_ARRANJO).ToList<Elemento>();
            return rs;
        }


        public Elemento getElementoArranjo(int idArranjo, int linha, int coluna)
        {
            var rs = rnBase.con.Table<Elemento>().Where(x => x.ID_ARRANJO == idArranjo && x.Linha == linha && x.Coluna == coluna).ToList<Elemento>();
            if (rs.Count() > 0)
            {
                elemento = rs[0];
                return rs[0];
            }
            else
                return null;
        }

    }
}
