﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EditorWin.Model;
namespace EditorWin.Controller
{
    class rnBaseController
    {
        public SQLite.SQLiteConnection con;
        public System.Data.SQLite.SQLiteConnection m_dbConnection;

        public bool EfetuarLogin(string login)
        {
           var resultado = con.Table<Usuario>().FirstOrDefault(x => x.Login == login && x.Perfil =="Professor");
           if ( resultado == null)
           {
               return false;
           }
           else
           {
               return true;
           }
         }

        public bool IsFirstAccess(string login)
        {
            var resultado = con.Table<Usuario>().FirstOrDefault(x => x.Login == login && x.Perfil == "Professor");
            if (resultado == null)
            {
                return false;
            }
            else
            {
                if (resultado.PrimeiroAcesso == "F")
                {
                    return false;
                }
                else
                {
                    resultado.PrimeiroAcesso = "F";
                    con.Update(resultado);
                    return true;
                }
            }
        }

        public rnBaseController()
        {
            SqlConnection cn = new SqlConnection();
            m_dbConnection = cn.getConnection();
            con = cn.conectar();
            con.CreateTable<Usuario>();
            con.CreateTable<Parametros>();
            con.CreateTable<Arranjo>();
            con.CreateTable<Elemento>();
        }
    }
}
