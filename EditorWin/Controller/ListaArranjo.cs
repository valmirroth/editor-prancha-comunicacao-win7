﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EditorWin.Model;
namespace EditorWin.Controller
{
    class ListaArranjo
    {
        private List<Arranjo> listArranjo;
        private Arranjo arranjoAtual;

        public ListaArranjo()
        {
            listArranjo = new List<Arranjo>();
            arranjoAtual = new Arranjo();
        }

        public void AddLista(List<Arranjo> lst)
        {
            listArranjo = lst;
            arranjoAtual = lst.First<Arranjo>();
        }
        public void limparLista()
        {
            listArranjo = null;
            arranjoAtual = null;
        }

        public Arranjo getArranjoAtual()
        {
            return arranjoAtual;
        }
    }
}
