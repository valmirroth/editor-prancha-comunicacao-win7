﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EditorWin.Model;
namespace EditorWin.Controller
{
    class ParametrosController
    {
        public rnBaseController rnBase;
        public Parametros userParametros;
        public ParametrosController(){
            rnBase = new rnBaseController();
            userParametros = new Parametros();
        }

        public Parametros getParametros(int idProfessor) {
            var result =  rnBase.con.Table<Parametros>().FirstOrDefault(x => x.ID_PROFESSOR == idProfessor);
            return result;
        }

        public void InserirParametro(Parametros pr)
        {
            var rs =   rnBase.con.Table<Parametros>().FirstOrDefault(x => x.ID_PROFESSOR == pr.ID_PROFESSOR);
            if (rs != null)
                rnBase.con.Execute(string.Format("Delete from {0} where ID_PROFESSOR = {1}", "Parametros",rs.ID_PROFESSOR));

            rnBase.con.Insert(pr);
        }
    }
}
