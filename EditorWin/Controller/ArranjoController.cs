﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EditorWin.Model;

namespace EditorWin.Controller
{
    class ArranjoController
    {

        public int linhas;
        public int colunas;

        private rnBaseController rnBase;
        private Arranjo arranjo;
        private ListaArranjo listaArranjo;

        public ArranjoController()
        {
            rnBase = new rnBaseController();
            listaArranjo = new ListaArranjo();
        }

        public bool CriarArranjo(Arranjo ar)
        {
            rnBase.con.Insert(ar);
            return true;
        }

        public List<Arranjo> getArranjos(int idUsuario)
        {
            var rs = rnBase.con.Table<Arranjo>().Where(x => x.ID_USUARIO == idUsuario).ToList<Arranjo>();
            return rs;
        }

        public bool getExistsArranjo(Arranjo ar)
        {
            var rs = rnBase.con.Table<Arranjo>().Where(x => x.ID_PROFESSOR == ar.ID_PROFESSOR && x.ID_USUARIO == ar.ID_USUARIO && x.NomeArranjo == ar.NomeArranjo);
            if (rs.Count() == 0)
                return false;
            else
                return true;
        }

        public bool AddArranjo(Arranjo ar)
        {
            rnBase.con.Insert(ar);
            return true;
        }

        public bool RemoveArranjo(Arranjo ar)
        {
            var rs = rnBase.con.Table<Arranjo>().Where(x => x.ID_USUARIO == ar.ID_USUARIO && x.ID_PROFESSOR == ar.ID_PROFESSOR && x.NomeArranjo == ar.NomeArranjo).ToList<Arranjo>();

            string sqlEle = "DELETE FROM Elemento WHERE ID_ARRANJO = " + rs[0].ID_ARRANJO +"";
            rnBase.con.Execute(sqlEle);

            string sql = "DELETE FROM Arranjo WHERE ID_PROFESSOR = " + ar.ID_PROFESSOR +" AND ID_USUARIO = " + ar.ID_USUARIO +" AND NomeArranjo = '" + ar.NomeArranjo +"'";
            rnBase.con.Execute(sql);
            return true;
        }

        public bool RemoveArranjo(int idUsuario)
        {
            string sql = " DELETE from Elemento where ID_ARRANJO IN (SELECT ID_ARRANJO FROM Arranjo where ID_USUARIO = " + idUsuario +")";
            rnBase.con.Execute(sql);
            sql = " DELETE FROM Arranjo where ID_USUARIO = " + idUsuario + "";
            rnBase.con.Execute(sql);
            return true;
        }

        public Arranjo getDadosArranjo(Arranjo ar)
        {
            var rs = rnBase.con.Table<Arranjo>().Where(x => x.ID_USUARIO == ar.ID_USUARIO && x.ID_PROFESSOR == ar.ID_PROFESSOR && x.NomeArranjo == ar.NomeArranjo).ToList<Arranjo>();
            if ( rs.Count() == 0 ) {
                listaArranjo.limparLista();
            }else
            {
                listaArranjo.AddLista(rs);
            }
            return listaArranjo.getArranjoAtual();
        }

        public int getIdArranjo(Arranjo ar)
        {
            var rs = rnBase.con.Table<Arranjo>().Where(x => x.ID_USUARIO == ar.ID_USUARIO && x.ID_PROFESSOR == ar.ID_PROFESSOR && x.NomeArranjo == ar.NomeArranjo).ToList<Arranjo>();
            if (rs.Count() == 0)
            {
                return rs[0].ID_ARRANJO;
            }
            else
            {
                return rs[0].ID_ARRANJO;
            }
        }
    }

}
