﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EditorWin.Controller
{
    class ImagemController
    {
        private rnBaseController rnBase;
        public IMAGEM img;
        public ImagemController()
        {
            rnBase = new rnBaseController();
            img = new IMAGEM();    
        }

        public void AddImagem(IMAGEM img)
        {
            rnBase.con.Insert(img);
        }

    }
}
