﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Data;
using EditorWin.Model;

namespace EditorWin.Controller
{
    class UsuarioController
    {
        rnBaseController rnBase;
        const string filename = @"C:\Mestrado\Db Imagens\db_Imagens.db3";

        public UsuarioController()
        {
            rnBase = new rnBaseController();
        }

        public DataSet ListaUsuarios(int idprofessor)
        {
            string sql;
            string appPath = System.IO.Directory.GetCurrentDirectory();
            appPath = appPath + "\\Db Imagens\\db_Imagens.db3";
            sql = "select Nome, Login, Perfil from Usuario where ID_PROFESSOR = " + idprofessor.ToString() +"";
            var conn = new SQLiteConnection("Data Source=" + appPath + ";Version=3;");
            DataSet ds = new DataSet();
            SqlConnection cn = new SqlConnection();
            var da = new SQLiteDataAdapter(sql, conn);
            da.Fill(ds);
            return ds;
        }

        public string InserirUsuario(Usuario us)
        {
            if (rnBase.con.Table<Usuario>().FirstOrDefault(x => x.Login == us.Login) == null) {
                {
                    rnBase.con.Insert(us);
                    ParametrosController pr = new ParametrosController();
                    Parametros pa = new Parametros();
                    pa.ID_PROFESSOR = rnBase.con.Table<Usuario>().FirstOrDefault(x => x.Login == us.Login).ID_USUARIO;
                    pa.Imprimir = false;
                    pr.InserirParametro(pa);
                    return "Usuário Cadastrado com sucesso.";
                }
            }
            else
            {
                return "Usuário já cadastrado.";
            }
            return "";
        }

        public int getIdUsuario(string sLogin)
        {
            return rnBase.con.Table<Usuario>().FirstOrDefault(x => x.Login == sLogin).ID_USUARIO;
        }

        public void RemoverUsuario(int idUsuario)
        {
            string sql = " DELETE from PARAMETROS where ID_PROFESSOR  = " + idUsuario + "";
            rnBase.con.Execute(sql);
            sql = " DELETE FROM Usuario where ID_USUARIO = " + idUsuario + "";
            rnBase.con.Execute(sql);
        }
    }
}
