﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;
using SQLite.Net.Interop;
using System.Data.SQLite;

namespace EditorWin.Controller
{
    class SqlConnection
    {
        public SQLite.SQLiteConnection sqlite_conn;

        public static System.Data.SQLite.SQLiteConnection m_dbConnection;
        public SqlConnection() {

        }

        public SQLite.SQLiteConnection conectar() {
            string appPath = System.IO.Directory.GetCurrentDirectory();
            appPath = appPath + "\\Db Imagens\\db_Imagens.db3";
            sqlite_conn = new SQLite.SQLiteConnection(appPath);
            m_dbConnection = new System.Data.SQLite.SQLiteConnection("Data Source="+ appPath + ";Version=3;");
            m_dbConnection.Open();
            return sqlite_conn;
        }

        public System.Data.SQLite.SQLiteConnection getConnection()
        {
            string appPath = System.IO.Directory.GetCurrentDirectory();
            appPath = appPath + "\\Db Imagens\\db_Imagens.db3";
            m_dbConnection = new System.Data.SQLite.SQLiteConnection("Data Source=" + appPath + ";Version=3;");
            m_dbConnection.Open();
            return m_dbConnection;
        }

        private SQLite.SQLiteConnection sql_con;
        private SQLite.SQLiteCommand sql_cmd;


        public void LoadData()
        {
            sql_con = conectar();
            string CommandText = "select * from IMAGEM";
            sql_cmd = sql_con.CreateCommand(CommandText);
            List<IMAGEM> ls;
            ls = new List<IMAGEM>();
            ls = sql_cmd.ExecuteQuery<IMAGEM>();
            int b = sql_cmd.ExecuteQuery<IMAGEM>().Count;
            SQLite.SQLite3.Result AD;
           sql_con.Close();
        }

        public List<T> executaQry<T>(string sql){
            sql_con = conectar();
            sql_cmd = sql_con.CreateCommand(sql);
            List<T> ls;
            ls = new List<T>();
            ls = sql_cmd.ExecuteQuery<T>();
            int b = sql_cmd.ExecuteQuery<T>().Count;
            SQLite.SQLite3.Result AD;
            sql_con.Close();
            List<T> a = new List<T>();
            return a;
            }

    }
}
